//Test file for loading seach result with least time execution.


let dictionary = [{
    word: 'Mobile',
    meaning: 'An electronic device with high calculation power',
    },
    {
        word: 'Arpegio',
        meaning: 'A progression of chords in music',
    },
    {
        word: 'Matchbox',
        meaning: 'A box which contains matchstick caplable of lighting up fire',
    },
    {
        word:'Choice',
        meaning: 'Decision taken by person on basis of two or more options present',
    }
];

// const index = dictionary.findIndex(function(mean, index){
// console.log(mean)
// return mean.word === 'Choice'
    
// })
// console.log(index);



//Method 1
// let findMeaning = function(mean, word){
//     const index = mean.findIndex(function(gotMean, index){
//         return gotMean.word.toLowerCase() === word.toLowerCase();
//     })

//     return mean[index]
// }

// let printMe = findMeaning(dictionary,'matchbox');
// console.log(printMe);


// Method 2
let findMeaning = function(myWord, word){
    const wordReturned = myWord.find(function(Mean,index){
        return Mean.word.toLowerCase() === word.toLowerCase();
    })
    return wordReturned
}

let printMe = findMeaning(dictionary,'matchbox');
console.log(printMe);